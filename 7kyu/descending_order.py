"""В этом модуле решена задача "Descending order"
https://www.codewars.com/kata/5467e4d82edf8bbf40000155
"""


def descending_quicksort(array: list) -> list:
    """Сортирует список по убыванию

    Args:
        array (list): Список объектов, которые можно сравнить друг с другом

    Returns:
        list: Отсортированный список
    """
    if len(array) < 2:
        return array
    less = []
    greater = []
    pivot = array[0]
    for i in array[1:]:
        if i < pivot:
            less.append(i)
        else:
            greater.append(i)
    return descending_quicksort(greater) + [pivot] + descending_quicksort(less)


def descending_order(num: int) -> int:
    """Сортирует цифры в числе по убыванию

    Args:
        num (int): Исходное число

    Returns:
        int: Число, в котором цифры отсортированы по невозрастанию
    """
    num = list(str(num))
    num = descending_quicksort(num)
    return int(''.join(num))
