"""В этом модуле решена задача "Shortest word"
https://www.codewars.com/kata/57cebe1dc6fdc20c57000ac9
"""

def find_short(line: str) -> int:
    """Вычисляет длину самого короткого слова в строке

    Args:
        line (str): исходная строка

    Returns:
        int: длина самого короткого слова
    """
    min_count = float('inf')
    current_count = 0
    for i in line:
        if i != ' ':
            current_count += 1
        else:
            if current_count < min_count:
                min_count = current_count
            current_count = 0
    if current_count < min_count:
        return current_count
    return min_count
